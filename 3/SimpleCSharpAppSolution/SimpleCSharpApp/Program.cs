﻿using System;

namespace SimpleCSharpApp
{
    class Program
    {
        static int Main(string[] args)
        {
            #region ShowSimpleMessage
            //Вывести пользователю простое сообщение
            Console.WriteLine("***** My First C# App *****");
            Console.WriteLine("Hello World!");
            Console.WriteLine();
            #endregion

            #region GetCommandLineArgsWithoutArgsOfMain
            //Принятие аргументов командой строки без принимающих параметров метода
            //string[] args = Environment.GetCommandLineArgs(); 
            #endregion

            #region ShowAllArgumentsMain
            for (int i = 0; i < args.Length; i++)
            {
                Console.WriteLine("Arg: {0}", args[i]);
            }
            #endregion

            #region ShowAllArgumentsMainWithHelpForeach
            //Обработать любые входные аргументы с помощью foreach
            //foreach (string arg in args)
            //{
            //    Console.WriteLine("Arg: {0}", arg);
            //}
            #endregion

            #region SubsidiaryMethod
            //Вспомогательный метод внутри класса Program.
            ShowEnvironmentDetails();
            #endregion

            #region WaitingClickEnter
            //Ожидать нажатия клавиши <Enter> перед завершением работы.
            Console.ReadLine();
            #endregion

            return -1;
        }

        private static void ShowEnvironmentDetails()
        {
            //Вывести информацию о дисковых устройствах
            //данной машины и другие интересные детали.
            foreach (string drive in Environment.GetLogicalDrives())
            {
                Console.WriteLine("Drive: {0}", drive);
            }

            Console.WriteLine("OS: {0}", Environment.OSVersion); //Операционная система
            Console.WriteLine("Number of processors: {0}", Environment.ProcessorCount); //Количество процессоров
            Console.WriteLine(".NET Version: {0}", Environment.Version); //Версия .NET
        }
    }
}
