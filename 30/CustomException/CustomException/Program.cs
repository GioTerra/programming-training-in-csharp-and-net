﻿using System;

namespace CustomException
{
    class Program
    {
        static void Main()
        {
            Console.WriteLine("***** Fun with Custom Exceptions *****");

            Car myCar = new Car("Rusty", 90);

            try
            {
                myCar.Accelerate(50);
            }
            catch (CarIsDeadException e)
            {
                Console.WriteLine(e.Message + "\n" +
                    e.ErrorTimeStamp + "\n" + 
                    e.CauseOfError);
            }

            Console.ReadLine();
        }
    }
}
