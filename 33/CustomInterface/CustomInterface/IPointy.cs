﻿using System;

namespace CustomInterface
{
    interface IPointy
    {
        byte Points { get; }
    }
}
