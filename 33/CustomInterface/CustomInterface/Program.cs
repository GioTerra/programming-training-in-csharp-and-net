﻿using System;

namespace CustomInterface
{
    class Program
    {
        static void Main()
        {
            Console.WriteLine("***** Fun with Interfaces *****\n");

            #region Using property Points
            Hexagon hex = new Hexagon();
            Console.WriteLine("Points: {0}", hex.Points);
            #endregion

            #region Catching an exception InvalidCastException
            Circle c = new Circle("Lisa");
            IPointy itfPt = null;

            try
            {
                itfPt = (IPointy)c;
                Console.WriteLine(itfPt.Points);
            }
            catch (InvalidCastException e)
            {
                Console.WriteLine(e.Message);
            }
            #endregion

            #region Using as with interface
            Hexagon hex2 = new Hexagon("Peter");
            IPointy itfPt2 = hex2 as IPointy;

            if (itfPt2 != null)
            {
                Console.WriteLine("Points: {0}", itfPt2.Points);
            }
            else
            {
                Console.WriteLine("OOOPs! Not pointy...");
            }
            #endregion

            #region Using is with interface
            Shape[] myShapes = { new Hexagon(), new Circle(), new Triangle("Joe"), new Circle("JoJo") };

            for (int i = 0; i < myShapes.Length; i++)
            {
                myShapes[i].Draw();

                if (myShapes[i] is IPointy)
                {
                    Console.WriteLine("-> Points: {0}", ((IPointy)myShapes[i]).Points);
                }
                else
                {
                    Console.WriteLine("-> {0}\'s not pointy!", myShapes[i].PetName);
                }

                if (myShapes[i] is IDraw3D)
                {
                    DrawIn3D((IDraw3D)myShapes[i]);
                }

                Console.WriteLine();
            }
            #endregion

            #region Passing to method reference on object that realizes interface IPointy
            IPointy firstPointyItem = FindFirstPointyShape(myShapes);
            Console.WriteLine("The item has {0} points", firstPointyItem.Points);
            #endregion;

            #region Use array of interface types
            IPointy[] myPointyObjects = { new Hexagon(), new Knife(), new Triangle(), new Fork(), new PitchFork() };

            foreach (IPointy i in myPointyObjects)
            {
                Console.WriteLine("Object has {0} points.", i.Points);
            } 
            #endregion

            Console.ReadLine();
        }

        static void DrawIn3D(IDraw3D itf3d)
        {
            Console.WriteLine("-> Drawing IDraw3D compatible type");

            itf3d.Draw3D();
        }

        static IPointy FindFirstPointyShape(Shape[] shapes)
        {
            foreach (Shape s in shapes)
            {
                if (s is IPointy)
                {
                    return s as IPointy;
                }
            }

            return null;
        }
    }

    class Knife : IPointy
    {
        public byte Points
        {
            get
            {
                return 2;
            }
        }
    }

    class Fork : IPointy
    {
        public byte Points
        {
            get
            {
                return 3;
            }
        }
    }

    class PitchFork : IPointy
    {
        public byte Points
        {
            get
            {
                return 4;
            }
        }
    }
}
