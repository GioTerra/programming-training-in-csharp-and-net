﻿using System;

namespace Employees
{
    class Program
    {
        static void Main()
        {
            Console.WriteLine("***** The Employee Class Hierarchy *****\n");

            #region Simple manipualtions with object and inherited members
            Salesperson fred = new Salesperson();
            fred.Age = 31;
            fred.Name = "Fred";
            fred.SalesNumber = 50;
            #endregion

            #region Simple manipulations with object which contain another object
            Manager chucky = new Manager("Chucky", 50, 92, 100000, "333-23-2322", 9000);
            double cost = chucky.GetBenefitCost();
            #endregion

            #region Using nested object
            Employee.BenefitPackage.BenefitPackageLevel myBenefirLevel = Employee.BenefitPackage.BenefitPackageLevel.Platinum;

            #endregion

            #region Using common inherited method for diverse objects
            chucky.GiveBonus(300);
            chucky.DisplayStats();
            Console.WriteLine();

            Salesperson fran = new Salesperson("Fran", 43, 93, 3000, "932-32-3232", 31);
            fran.GiveBonus(200);
            fran.DisplayStats(); 
            #endregion

            Console.ReadLine();
        }

        static void CastingExamples()
        {
            object frank = new Manager("Frank Zappa", 9, 3000, 40000, "111-11-1111", 5);

            GivePromotion((Manager)frank);

            Employee moonUnit = new Manager("MoonUnit Zappa", 2, 3001, 20000, "101-11-1231", 1);

            GivePromotion(moonUnit);

            Salesperson jill = new PTSalesPerson("Jill", 834, 3002, 100000, "111-12-1119", 90);

            GivePromotion(jill);

            object empS = new Salesperson();
            Manager empM = empS as Manager;

            if (empM == null)
            {
                Console.WriteLine("Sorry, empS isn't a Manager");
            }
        }

        static void GivePromotion(Employee emp)
        {
            Console.WriteLine("{0} was promoted!", emp.Name);

            if (emp is Salesperson)
            {
                Console.WriteLine("{0} made {1} sale(s)!\n", emp.Name, ((Salesperson)emp).SalesNumber);
            }

            if (emp is Manager)
            {
                Console.WriteLine("{0} had {1} stock options...\n", emp.Name, ((Manager)emp).StockOptions);
            }
        }
    }
}
