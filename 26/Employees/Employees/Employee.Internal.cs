﻿using System;

namespace Employees
{
    partial class Employee
    {
        #region Data Members
        protected string empName;
        protected int empID;
        protected float currPay;
        protected int empAge;
        protected string empSSN;
        protected BenefitPackage empBenefits = new BenefitPackage();
        #endregion

        #region Constructors
        public Employee() { }

        public Employee(string name, int id, float pay)
            : this(name, 0, id, pay, null) { }

        public Employee(string name, int age, int id, float pay, string ssn)
        {
            Name = name;
            ID = id;
            Pay = pay;
            Age = age;
            empSSN = ssn;
        }
        #endregion
    }
}
