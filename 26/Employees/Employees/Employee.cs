﻿using System;

namespace Employees
{
    abstract partial class Employee
    {
        #region Properties
        public BenefitPackage Benefits
        {
            get
            {
                return empBenefits;
            }
            set
            {
                empBenefits = value;
            }
        }

        public int Age
        {
            get
            {
                return empAge;
            }
            set
            {
                empAge = value;
            }
        }

        public string Name
        {
            get
            {
                return empName;
            }
            set
            {
                if (value.Length > 15)
                {
                    Console.WriteLine("\nError! Name must be less than 16 characters!\n\n");
                }
                else
                {
                    empName = value;
                }
            }
        }

        public int ID
        {
            get
            {
                return empID;
            }
            set
            {
                empID = value;
            }
        }

        public float Pay
        {
            get
            {
                return currPay;
            }
            set
            {
                currPay = value;
            }
        }

        public string SocialSecurityNumber
        {
            get
            {
                return empSSN;
            }
        }
        #endregion

        #region Traditional get and set methods
        public string GetName()
        {
            return empName;
        }

        public void SetName(string name)
        {
            if (name.Length > 15)
            {
                Console.WriteLine("\nError! Name must be less than 16 characters!\n\n");
            }
            else
            {
                empName = name;
            }
        } 
        #endregion

        #region Methods
        public virtual void GiveBonus(float amount)
        {
            Pay += amount;
        }

        public virtual void DisplayStats()
        {
            Console.WriteLine("Name: {0}\n" +
               "ID: {1}\n" +
               "Pay: {2}\n" +
               "Age: {3}\n\n", Name, ID, Pay, Age);
        }

        public double GetBenefitCost()
        {
            return empBenefits.ComputePayDeduction();
        }
        #endregion

        public class BenefitPackage
        {
            public enum BenefitPackageLevel
            {
                Standart,
                Gold,
                Platinum
            }

            public double ComputePayDeduction()
            {
                return 125.0;
            }
        }
    }
}
