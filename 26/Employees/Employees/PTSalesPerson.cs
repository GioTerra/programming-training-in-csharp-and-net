﻿using System;

namespace Employees
{
    sealed class PTSalesPerson : Salesperson
    {
        public PTSalesPerson(string fullName, int age, int empID, float currPay, string ssn, int numbOfSales)
            : base(fullName, age, empID, currPay, ssn, numbOfSales)
        { }
    }
}
