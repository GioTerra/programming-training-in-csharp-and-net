﻿using System;
using System.IO;

namespace ProcessMultipleExceptions
{
    class Program
    {
        static void Main()
        {
            Console.WriteLine("***** Handling Multiple Exceptions *****\n");

            Car myCar = new Car("Rusty", 90);

            try
            {
                myCar.Accelerate(-10);
            }
            catch (CarIsDeadException e)
            {
                Console.WriteLine(e.Message);
            }
            catch (ArgumentOutOfRangeException e)
            {
                Console.WriteLine(e.Message);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            Car yourCar = new Car("Dusty", 90);

            try
            {
                yourCar.Accelerate(90);
            }
            catch
            {
                Console.WriteLine("Something bad happened...");
            }

            Car ourCar = new Car("Gusty", 90);

            try
            {
                ourCar.Accelerate(90);
            }
            catch (CarIsDeadException e)
            {
                //throw;
            }

            Car herCar = new Car("Nusty", 90);

            try
            {
                herCar.Accelerate(90);
            }
            catch (CarIsDeadException e)
            {
                try
                {
                    FileStream fs = File.Open(@"C:\carErrors.txt", FileMode.Open);
                }
                catch (Exception e2)
                {
                    //throw new CarIsDeadException(e.Message, e2);
                }
            }

            Car hisCar = new Car("Pusty", 90);
            hisCar.CrankTunes(true);

            try
            {
                hisCar.Accelerate(90);
            }
            catch (CarIsDeadException e)
            { }
            catch (ArgumentOutOfRangeException e)
            { }
            catch (Exception e)
            { }
            finally
            {
                myCar.CrankTunes(false);
            }

            Car theirCar = new Car("Lusty", 90);
            theirCar.Accelerate(90);

            Console.ReadLine();
        }
    }
}
