﻿using System;
using System.Collections;

namespace SimpleException
{
    class Program
    {
        static void Main()
        {
            Console.WriteLine("***** Simple Exception Example *****");
            Console.WriteLine("=> Creating a car and stepping on it!");

            Car myCar = new Car("Zippy", 20);
            myCar.CrankTunes(true);

            try
            {
                for (int i = 0; myCar.CurrentSpeed != 0; i++)
                {
                    myCar.Accelerate(10);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("\n\n*** Error! ***\n" +
                    "Member name: {0}\n" +
                    "Class defining member: {3}\n" +
                    "Member type: {4}\n" +
                    "Message: {1}\n" +
                    "Source: {2}\n" +
                    "Stack: {5}\n" +
                    "Help link: {6}",
                    e.TargetSite, e.Message, e.Source,
                    e.TargetSite.DeclaringType, e.TargetSite.MemberType, e.StackTrace,
                    e.HelpLink);

                Console.WriteLine("\n\n-> Custom Data:");

                if (e.Data != null)
                {
                    foreach (DictionaryEntry de in e.Data)
                    {
                        Console.WriteLine("-> {0}: {1}", de.Key, de.Value);
                    }
                }
            }

            Console.WriteLine("\n ***** Out of exception logic *****");

            Console.ReadLine();
        }
    }
}
