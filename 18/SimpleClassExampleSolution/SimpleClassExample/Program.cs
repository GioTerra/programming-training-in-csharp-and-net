﻿using System;

namespace SimpleClassExample
{
    class Program
    {
        static void Main()
        {
            Console.WriteLine("***** Fun with Class Types *****\n");

            #region SimpleManipulationsWithObject
            Car myCar = new Car();
            myCar.petName = "Henry";
            myCar.currSpeed = 10;

            for (int i = 0; i <= 10; i++)
            {
                myCar.SpeedUp(5);
                myCar.PrintState();
            }

            Console.WriteLine();
            #endregion

            #region UsingDiverseConstructors
            Car chuck = new Car();
            chuck.PrintState();

            Car mary = new Car("Mary");
            mary.PrintState();

            Car daisy = new Car("Daisy", 75);
            daisy.PrintState();

            Console.WriteLine();
            #endregion

            Motorcycle mc = new Motorcycle();
            mc.PopAWheely();

            Motorcycle c = new Motorcycle(5);
            c.SetDriverName("Tiny");
            c.PopAWheely();
            Console.WriteLine("Rider name is {0}", c.driverName);

            Console.ReadLine();
        }
    }
}
