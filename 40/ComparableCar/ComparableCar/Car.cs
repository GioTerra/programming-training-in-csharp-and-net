﻿using System;
using System.Collections;

namespace ComparableCar
{
    class Car : IComparable
    {
        public const int MaxSpeed = 100;

        public int CurrentSpeed { get; set; }
        public string PetName { get; set; }
        public int CarID { get; set; }
        public static IComparer SortByPetName
        {
            get
            {
                return (IComparer)new PetNameComparer();
            }
        }

        private bool carIsDead;
        
        public Car()
        {

        }

        public Car(string name, int speed)
        {
            CurrentSpeed = speed;
            PetName = name;
        }

        public Car(string name, int currSp, int id)
        {
            CurrentSpeed = currSp;
            PetName = name;
            CarID = id;
        }

        public void Accelerate(int delta)
        {
            if (carIsDead)
            {
                Console.WriteLine("{0} is out of order...", PetName);
            }
            else
            {
                CurrentSpeed += delta;
                if (CurrentSpeed > MaxSpeed)
                {
                    CurrentSpeed = 0;
                    carIsDead = true;

                    Exception ex = new Exception(string.Format("{0} has overheated!", PetName));
                    ex.HelpLink = "http://www.car.com";

                    ex.Data.Add("TimeStamp", string.Format("The car exploded at {0}", DateTime.Now));
                    ex.Data.Add("Cause", "You have a lead foot.");

                    throw ex;
                }
                else
                {
                    Console.WriteLine("=> CurrentSpeed = {0}", CurrentSpeed);
                }
            }
        }

        int IComparable.CompareTo(object obj)
        {
            Car temp = obj as Car;

            if (temp != null)
            {
                //if (this.CarID > temp.CarID)
                //{
                //    return 1;
                //}

                //if (this.CarID < temp.CarID)
                //{
                //    return -1;
                //}
                //else return 0;

                return this.CarID.CompareTo(temp.CarID);
            }
            else
            {
                throw new ArgumentException("Parameter is not a Car!");
            }
        }
    }
}
