﻿using System;

namespace SimpleUtilityClass
{
    class Program
    {
        static void Main()
        {
            Console.WriteLine("***** Fun with Static Classes *****");

            TimeUtilClass.PrintDate();
            TimeUtilClass.PrintTime();

            Console.ReadLine();
        }
    }
}
