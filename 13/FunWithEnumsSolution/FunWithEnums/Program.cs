﻿using System;

namespace FunWithEnums
{
    class Program
    {
        static void Main()
        {
            Console.WriteLine("***** Fun with Enums *****\n\n");

            EmpType emp = EmpType.Contractor;
            AskForBonus(emp);

            Console.WriteLine("\n\nEmpType uses a {0} for storage", Enum.GetUnderlyingType(emp.GetType()));
            Console.WriteLine("EmpType uses a {0} for storage\n\n", Enum.GetUnderlyingType(typeof(EmpType)));

            Console.WriteLine("emp is a {0}.\n\n", emp.ToString());
            Console.WriteLine("{0} = {1}\n\n", emp.ToString(), (byte)emp);

            EmpType e2 = EmpType.Contractor;
            DayOfWeek day = DayOfWeek.Monday;
            ConsoleColor cc = ConsoleColor.Gray;

            EvaluateEnum(e2);
            EvaluateEnum(day);
            EvaluateEnum(cc);

            Console.ReadLine();
        }

        private static void EvaluateEnum(System.Enum e)
        {
            Console.WriteLine("\n=> Information about {0}", e.GetType().Name);

            Console.WriteLine("Underlying storage type: {0}", Enum.GetUnderlyingType(e.GetType()));

            Array enumData = Enum.GetValues(e.GetType());
            Console.WriteLine("This enum has {0} members.", enumData.Length);

            for (int i = 0; i < enumData.Length; i++)
            {
                Console.WriteLine("Name: {0}, Value: {0:D}", enumData.GetValue(i));
            }
        }

        private static void AskForBonus(EmpType e)
        {
            switch (e)
            {
                case EmpType.Manager:
                    Console.WriteLine("How about stock options instead?");
                    break;
                case EmpType.Grunt:
                    Console.WriteLine("You have got to be kidding...");
                    break;
                case EmpType.Contractor:
                    Console.WriteLine("You already get enough cash...");
                    break;
                case EmpType.VicePresident:
                    Console.WriteLine("Very good, sir!");
                    break;
                default:
                    break;
            }
        }
    }

    enum EmpType : Byte
    {
        Manager = 10,
        Grunt = 1,
        Contractor = 100,
        VicePresident = 9
    }


}
