﻿using System;

namespace EmployeeApp
{
    class Program
    {
        static void Main()
        {
            Console.WriteLine("***** Fun with Encapsulation *****\n\n");

            #region Simple working with object
            Employee emp = new Employee("Marvin", 456, 30000);
            emp.GiveBonus(1000);
            emp.DisplayStats();
            #endregion

            #region Working with traditional get and set methods
            emp.SetName("Marv");
            Console.WriteLine("\nEmployee is named: {0}", emp.GetName());
            #endregion

            #region Simple working with property
            emp.Name = "Markusha";
            Console.WriteLine("\nEmployee is named: {0}", emp.Name);
            #endregion

            #region Simple manipulations with ready class
            Employee emp2 = new Employee();
            emp2.SetName("Xena the warrior princess");
            #endregion

            #region Compare traditional methods with property
            Employee joe = new Employee();
            joe.SetName(joe.GetName() + 1);
            joe.Age++; 
            #endregion

            Console.ReadLine();
        }
    }
}
