﻿using System;

namespace EmployeeApp
{
    partial class Employee
    {
        #region Data Members
        private string empName;
        private int empID;
        private float currPay;
        private int empAge;
        private string empSSN;
        #endregion

        #region Constructors
        public Employee() { }

        public Employee(string name, int id, float pay)
            : this(name, 0, id, pay, null) { }

        public Employee(string name, int age, int id, float pay, string ssn)
        {
            Name = name;
            ID = id;
            Pay = pay;
            Age = age;
            empSSN = ssn;
        }
        #endregion
    }
}
