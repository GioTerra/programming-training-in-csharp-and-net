﻿using System;

namespace MIInterfaceHierarchy
{
    interface IPrintable
    {
        void Print();
        void Draw();
    }
}
