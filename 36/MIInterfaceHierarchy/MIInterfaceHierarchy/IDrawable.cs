﻿using System;

namespace MIInterfaceHierarchy
{
    interface IDrawable
    {
        void Draw();
    }
}
