﻿using System;

namespace MIInterfaceHierarchy
{
    class Square : IShape
    {
        void IPrintable.Draw() { }
        void IDrawable.Draw() { }
        public void Print() { }

        public int GetNumberOfSides()
        {
            return 4;
        }
    }
}
