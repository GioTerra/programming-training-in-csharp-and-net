﻿using System;
using System.Linq;
using System.Text;

namespace ImplicitlyTypedLocalVars
{
    class Program
    {
        static void Main()
        {
            LinqQueryOverInts();
            DeclareImplicitVars();
        }

        private static void LinqQueryOverInts()
        {
            int[] numbers = { 10, 20, 30, 40, 1, 2, 3, 8 };
            var subset = from i in numbers where i < 10 select i;

            Console.WriteLine("Values in subset: ");

            foreach (var i in subset)
            {
                Console.WriteLine("{0} ", i);
            }

            Console.WriteLine("\n");

            Console.WriteLine("subset is a: {0}", subset.GetType().Name);
            Console.WriteLine("subset is defined in: {0}", subset.GetType().Namespace);

            Console.ReadLine();
            Console.Clear();
        }

        private static void DeclareImplicitVars()
        {
            Console.WriteLine("***** Fun with Implicit Typing *****");

            var myInt = 0;
            var myBool = true;
            var myString = "Time, marches on...";
            var var = 'v';
            var myStringBuilder = new StringBuilder("123");

            Console.WriteLine("myInt is a: {0}", myInt.GetType().Name);
            Console.WriteLine("myBool is a: {0}", myBool.GetType().Name);
            Console.WriteLine("myString is a: {0}", myString.GetType().Name);
            Console.WriteLine("myStringBuilder is a: {0}", myStringBuilder.GetType().Name);

            Console.ReadLine();
            Console.Clear();
        }
    }
}
