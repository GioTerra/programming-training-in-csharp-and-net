﻿using System;

namespace Methodoverloading
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("***** Fun with Method Overloading *****\n\n");

            Console.WriteLine(Add(10, 10));
            Console.WriteLine(Add(9000000000, 9000000000));
            Console.WriteLine(Add(4.3, 4.4));

            Console.ReadLine();
        }

        static int Add(int x, int y)
        {
            return x + y;
        }

        static double Add(double x, double y)
        {
            return x + y;
        }

        static long Add(long x, long y)
        {
            return x + y;
        }
    }
}
