﻿using System;

namespace InterfaceNameClash
{
    class Octagon : IDrawToForm, IDrawToMemory, IDrawToPrinter
    {
        void IDrawToForm.Draw()
        {
            Console.WriteLine("Drawing the form...");
        }

        void IDrawToMemory.Draw()
        {
            Console.WriteLine("Drawing the memory...");
        }

        void IDrawToPrinter.Draw()
        {
            Console.WriteLine("Drawing the printer...");
        }
    }
}
