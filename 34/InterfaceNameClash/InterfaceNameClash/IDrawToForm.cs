﻿using System;

namespace InterfaceNameClash
{
    interface IDrawToForm
    {
        void Draw();
    }
}
