﻿using System;

namespace InterfaceNameClash
{
    class Program
    {
        static void Main()
        {
            Console.WriteLine("***** Fun with Interface Name Clashes *****\n");

            Octagon oct = new Octagon();

            IDrawToForm itfForm = (IDrawToForm)oct;
            itfForm.Draw();

            ((IDrawToPrinter)oct).Draw();

            if (oct is IDrawToMemory)
            {
                (oct as IDrawToMemory).Draw();
            }

            Console.ReadLine();
        }
    }
}
