﻿using System;

namespace InterfaceNameClash
{
    interface IDrawToPrinter
    {
        void Draw();
    }
}
