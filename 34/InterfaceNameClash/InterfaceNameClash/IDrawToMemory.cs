﻿using System;

namespace InterfaceNameClash
{
    interface IDrawToMemory
    {
        void Draw();
    }
}
