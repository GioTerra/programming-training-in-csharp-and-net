﻿using System;

namespace ConstData
{
    class Program
    {
        static void Main()
        {
            Console.WriteLine("***** Fun with Const *****\n");

            Console.WriteLine("\nThe value of PI is: {0}", MyMathClass.pi);

            Console.ReadLine();
        }
    }

    class MyMathClass
    {
        public const double pi = 3.14;
        public readonly int historicalDate;
        public static readonly string organizationName;

        public MyMathClass()
        {
            historicalDate = 1993;
        }

        static MyMathClass()
        {
            organizationName = "DC";
        }

        static void LocalConstStringVariable()
        {
            const string fixedStr = "Fixed string Data";
            Console.WriteLine(fixedStr);
        }
    }
}
