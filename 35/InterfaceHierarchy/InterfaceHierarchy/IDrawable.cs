﻿using System;

namespace InterfaceHierarchy
{
    public interface IDrawable
    {
        void Draw();
    }
}
