﻿using System;

namespace InterfaceHierarchy
{
    class Program
    {
        static void Main()
        {
            Console.WriteLine("***** Simple Interface Hierarchy *****");

            BitmapImage myBitmap = new BitmapImage();
            myBitmap.Draw();
            myBitmap.DrawInBoundingBox(10, 10, 100, 150);
            myBitmap.DrawUpsideDown();

            IAdvancedDraw iAdvDraw = myBitmap as IAdvancedDraw;
            if (iAdvDraw != null)
            {
                iAdvDraw.DrawUpsideDown();
            }

            Console.ReadLine();
        }
    }
}
