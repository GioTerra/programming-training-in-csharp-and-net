﻿using System;

namespace ObjectInitializers
{
    class Point
    {
        public int X { get; set; }
        public int Y { get; set; }
        public PointColor Color { get; set; }

        public Point(PointColor ptColor)
        {
            Color = ptColor;
        }

        public Point(int xVal, int yVal)
        {
            X = xVal;
            Y = yVal;
        }

        public Point()
            : this(PointColor.BloodRed)
        {

        }

        public void DisplayStats()
        {
            Console.WriteLine("[{0}, {1}]\n" +
                "Point is {2}", X, Y, Color);
        }
    }

    public enum PointColor
    {
        LightBLue,
        BloodRed,
        Gold
    }
}
