﻿using System;

namespace ObjectInitializers
{
    class Program
    {
        static void Main()
        {
            Console.WriteLine("***** Fun with Object Init Syntax *****\n\n");

            Point firstPoint = new Point();
            firstPoint.X = 10;
            firstPoint.Y = 10;
            firstPoint.DisplayStats();

            Point secondPoint = new Point(20, 20);
            secondPoint.DisplayStats();

            Point finalPoint = new Point { X = 30, Y = 30 };
            finalPoint.DisplayStats();

            Point pt = new Point(400, 400) { X = 40, Y = 40 };

            Point goldPoint = new Point(PointColor.Gold) { X = 50, Y = 50 };
            goldPoint.DisplayStats();

            Rectangle myRect = new Rectangle
            {
                TopLeft = new Point { X = 10, Y = 10 },
                BottomRight = new Point { X = 200, Y = 200 }
            };

            Rectangle r = new Rectangle();
            r.TopLeft = new Point() { X = 10, Y = 10 };
            r.BottomRight = new Point();
            r.BottomRight.X = 20;
            r.BottomRight.Y = 20;

            Console.ReadLine();
        }
    }
}
