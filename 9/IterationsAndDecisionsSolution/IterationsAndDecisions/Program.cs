﻿using System;

namespace IterationsAndDecisions
{
    class Program
    {
        static void Main()
        {
            SwitchOnEnumExample();
            SwitchOnStringExample();
            SwitchExample();
            IfElseExample();
            DoWhileLoopExample();
            WhileLoopExample();
            ForEachLoopExample();
            ForLoopExample();
        }

        private static void SwitchOnEnumExample()
        {
            Console.Write("Enter your favorite day of the week: ");

            DayOfWeek favDay = new DayOfWeek();

            try
            {
                favDay = (DayOfWeek)Enum.Parse(typeof(DayOfWeek), Console.ReadLine());
            }
            catch (Exception)
            {
                Console.WriteLine("Bad input!");
                Console.ReadLine();
                return;
            }

            switch (favDay)
            {
                case DayOfWeek.Sunday:
                    Console.WriteLine("Football!!!");
                    break;
                case DayOfWeek.Monday:
                    Console.WriteLine("Another day, another dollar.");
                    break;
                case DayOfWeek.Tuesday:
                    Console.WriteLine("At least it is not Monday.");
                    break;
                case DayOfWeek.Wednesday:
                    Console.WriteLine("A fine day.");
                    break;
                case DayOfWeek.Thursday:
                    Console.WriteLine("Almost Friday...");
                    break;
                case DayOfWeek.Friday:
                    Console.WriteLine("Yes, Friday rules!");
                    break;
                case DayOfWeek.Saturday:
                    Console.WriteLine("Great day indeed.");
                    break;
            }

            Console.ReadLine();
            Console.Clear();
        }

        private static void SwitchOnStringExample()
        {
            Console.WriteLine("C# or VB");
            Console.Write("Please pick your language preference: ");

            string langChoice = Console.ReadLine();

            switch (langChoice.ToLower())
            {
                case "c#":
                    Console.WriteLine("Good choice, C# is a fine language.");
                    break;
                case "vb":
                    Console.WriteLine("VB: OOP, multithreading and more!");
                    break;
                default:
                    Console.WriteLine("Well... good luck with that!");
                    break;
            }

            Console.ReadLine();
            Console.Clear();
        }

        private static void SwitchExample()
        {
            Console.WriteLine("1 [C#], 2 [VB]");
            Console.Write("Please pick your language preference: ");

            string langChoice = Console.ReadLine();
            int n = int.Parse(langChoice);

            switch (n)
            {
                case 1:
                    Console.WriteLine("Good choice, C# is a fine language.");
                    break;
                case 2:
                    Console.WriteLine("VB: OOP, multithreading, and more!");
                    break;
                default:
                    Console.WriteLine("Well... good luck with that!");
                    break;
            }

            Console.ReadLine();
            Console.Clear();
        }

        private static void IfElseExample()
        {
            string stringData = "My textual data";

            if (stringData.Length > 0)
            {
                Console.WriteLine("string is greater than 0 characters");
            }

            Console.ReadLine();
            Console.Clear();
        }

        private static void DoWhileLoopExample()
        {
            string userIsDone = "";

            do
            {
                Console.WriteLine("In do/while loop");
                Console.Write("Are you ready? [yes] or [no]: ");
                userIsDone = Console.ReadLine();
            } while (userIsDone.ToLower() != "yes");

            Console.ReadLine();
            Console.Clear();
        }

        private static void WhileLoopExample()
        {
            string userIsDone = "";

            while (userIsDone.ToLower() != "yes")
            {
                Console.WriteLine("In while loop");
                Console.Write("Are you ready? [yes] or [no]: ");
                userIsDone = Console.ReadLine();
            }

            Console.ReadLine();
            Console.Clear();
        }

        private static void ForEachLoopExample()
        {
            string[] carTypes = { "Ford", "BMW", "Yugo", "Honda" };

            foreach (string c in carTypes)
            {
                Console.WriteLine(c);
            }

            int[] myInts = { 10, 20, 30, 40 };

            foreach (int i in myInts)
            {
                Console.WriteLine(i);
            }

            Console.ReadLine();
            Console.Clear();
        }

        private static void ForLoopExample()
        {
            for (int i = 0; i < 4; i++)
            {
                Console.WriteLine("Number is: {0}", i);
            }

            Console.ReadLine();
            Console.Clear();
        }
    }
}
