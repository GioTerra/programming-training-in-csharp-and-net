﻿using System;

namespace TypeConversions
{
    class Program
    {
        static void Main()
        {
            Console.WriteLine("***** Fun with type conversions *****");

            ProcessBytes();
            NarrowingAttempt2();
            NarrowingAttempt();
        }

        private static void ProcessBytes()
        {
            byte b1 = 100;
            byte b2 = 250;

            try
            {
                byte sum = checked(unchecked((byte)Add(b1, b2)));

                //checked
                //{
                //    byte sum = (byte)Add(b1, b2);
                //    Console.WriteLine("sum = {0}", sum);
                //}

                Console.WriteLine("sum = {0}", sum);
            }
            catch (OverflowException ex)
            {
                Console.WriteLine(ex.Message);
            }

            Console.ReadLine();
            Console.Clear();
        }

        private static void NarrowingAttempt2()
        {
            try
            {
                short numb1 = 30000, numb2 = 30000;
                short answer = (short)Add(numb1, numb2);
                Console.WriteLine("{0} + {1} = {2}", numb1, numb2, answer);
            }
            catch (OverflowException ex)
            {
                Console.WriteLine(ex.Message);
            }
            
            Console.ReadLine();
            Console.Clear();
        }

        private static void NarrowingAttempt()
        {
            byte myByte = 0;
            int myInt = 200;

            myByte = (byte)myInt;

            Console.WriteLine("Value of myByte: {0}", myByte);

            Console.ReadLine();
            Console.Clear();
        }

        private static int Add(int x, int y)
        {
            return x + y;
        }
    }
}
