﻿using System;

namespace CloneablePoint
{
    class Program
    {
        static void Main()
        {
            Console.WriteLine("***** Fun with Object Cloning *****");

            Point p1 = new Point(50, 50);
            Point p2 = p1;

            p2.X = 0;

            Console.WriteLine(p1 + "\n" + p2);

            Point p3 = new Point(100, 100);
            Point p4 = (Point)p3.Clone();

            p4.X = 0;

            Console.WriteLine(p3 + "\n" + p4);

            Console.WriteLine("Cloned p5 and stored new Point in p6");

            Point p5 = new Point(100, 100, "Jane");
            Point p6 = (Point)p5.Clone();

            Console.WriteLine("Before modifications:\n" +
                "p5: {0}\n" +
                "p6: {1}\n", p5, p6);

            p6.desc.PetName = "My new Point";
            p6.X = 9;

            Console.WriteLine("Changed p6.desc.petName and p6.X:\n" +
                "p5: {0}\n" +
                "p6: {1}\n", p5, p6);

            Console.ReadLine();
        }
    }
}
