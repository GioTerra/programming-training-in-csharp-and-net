﻿using System;

namespace CloneablePoint
{
    class Point : ICloneable
    {
        public int X { get; set; }
        public int Y { get; set; }
        public PointDescription desc = new PointDescription();

        public Point() { }

        public Point(int xPos, int yPos)
        {
            X = xPos;
            Y = yPos;
        }

        public Point(int xPos, int yPos, string petName)
        {
            X = xPos;
            Y = yPos;
            desc.PetName = petName;
        }

        public override string ToString()
        {
            return string.Format("X = {0}; Y = {1}; Name = {2}; ID = {3}\n", X, Y, desc.PetName, desc.PointID);
        }

        public object Clone()
        {
            Point newPoint = (Point)this.MemberwiseClone();

            PointDescription currentDesc = new PointDescription();
            currentDesc.PetName = this.desc.PetName;
            newPoint.desc = currentDesc;

            //newPoint.desc = new PointDescription() { PetName = this.desc.PetName };

            return newPoint;
        }
    }
}
