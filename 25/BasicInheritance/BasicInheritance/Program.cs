﻿using System;

namespace BasicInheritance
{
    class Program
    {
        static void Main()
        {
            Console.WriteLine("***** Basic Inheritance *****\n");

            #region Simple use of class Car
            Car myCar = new Car(80);
            myCar.Speed = 50;

            Console.WriteLine("My car is going {0} MPH", myCar.Speed);
            #endregion

            #region Simple use of class MiniVan with inheritance
            MiniVan myVan = new MiniVan();
            myVan.Speed = 10;
            Console.WriteLine("My van is going {0} MPH", myVan.Speed); 
            #endregion

            Console.ReadLine();
        }
    }
}
