﻿using System;
using System.Numerics;

namespace BasicDataTypes
{
    class Program
    {
        static void Main()
        {
            UseBigInteger();
            UseDatesAndTimes();
            ParseFromStrings();
            CharFinctionality();
            DataTypeFunctionality();
            ObjectFunctionality();
            LocalVarDeclarations();
            NewingDataTypes();
        }

        private static void UseBigInteger()
        {
            Console.WriteLine("=> Use BigInteger:");

            BigInteger biggy = BigInteger.Parse("99999999999999999999999999999999999999999999999999999");
            Console.WriteLine("Value of biggy is {0}", biggy);
            Console.WriteLine("Is biggy an even value?: {0}", biggy.IsEven);
            Console.WriteLine("Is biggy a power of two?: {0}", biggy.IsPowerOfTwo);
            BigInteger reallyBig = BigInteger.Multiply(biggy, 
                BigInteger.Parse("88888888888888888888888888888888888888888888888888888888888888888888"));
            Console.WriteLine("Value of reallyBig is {0}", reallyBig);

            Console.ReadLine();
            Console.Clear();
        }

        private static void UseDatesAndTimes()
        {
            Console.WriteLine("=> Dates and Times:");

            DateTime dt = new DateTime(2011, 10, 17);

            Console.WriteLine("The day of {0} is {1}", dt.Date, dt.DayOfWeek);

            dt = dt.AddMonths(2);
            Console.WriteLine("Daylight savings: {0}", dt.IsDaylightSavingTime());

            TimeSpan ts = new TimeSpan(4, 30, 0);
            Console.WriteLine(ts);

            Console.WriteLine(ts.Subtract(new TimeSpan(0, 15, 0)));

            Console.ReadLine();
            Console.Clear();
        }

        private static void ParseFromStrings()
        {
            Console.WriteLine("=> Data type parsing:");

            bool b = bool.Parse("True");
            Console.WriteLine("Value of b: {0}", b);

            double d = double.Parse("99,884");
            Console.WriteLine("Value of d: {0}", d);

            int i = int.Parse("8");
            Console.WriteLine("Value of i: {0}", i);

            char c = char.Parse("w");
            Console.WriteLine("Value of c: {0}", c);

            Console.ReadLine();
            Console.Clear();
        }

        private static void CharFinctionality()
        {
            Console.WriteLine("=> Char type Functionality:");

            char myChar = 'a';
            Console.WriteLine("char.IsDigit('a'): {0}", char.IsDigit(myChar));
            Console.WriteLine("char.IsLetter('a'): {0}", char.IsLetter(myChar));
            Console.WriteLine("char.IsWhiteSpace('Hello There', 5): {0}", char.IsWhiteSpace("Hello There", 5));
            Console.WriteLine("char.IsWhiteSpace('Hello There', 6): {0}", char.IsWhiteSpace("Hello There", 6));
            Console.WriteLine("char.IsPunctuation('?'): {0}", char.IsPunctuation('?'));

            Console.ReadLine();
            Console.Clear();
        }

        private static void DataTypeFunctionality()
        {
            Console.WriteLine("=> Data type Functionality:");

            Console.WriteLine("Max of int: {0}", int.MaxValue);
            Console.WriteLine("Min of int: {0}", System.Int32.MinValue);
            Console.WriteLine("Max of double: {0}", double.MaxValue);
            Console.WriteLine("Min of double: {0}", double.MinValue);
            Console.WriteLine("double.Epsilon: {0}", double.Epsilon);
            Console.WriteLine("double.PositiveInfinity: {0}", double.PositiveInfinity);
            Console.WriteLine("double.NegativeInfinity: {0}", double.NegativeInfinity);

            Console.ReadLine();
            Console.Clear();
        }

        private static void ObjectFunctionality()
        {
            Console.WriteLine("=> System.Object Functionality:");

            Console.WriteLine("12.GetHashCode() = {0}", 12.GetHashCode());
            Console.WriteLine("12.Equals(23) = {0}", 12.Equals(23));
            Console.WriteLine("12.ToString() = {0}", 12.ToString());
            Console.WriteLine("12.GetType() = {0}", 12.GetType());
            
            Console.ReadLine();
            Console.Clear();
        }

        private static void NewingDataTypes()
        {
            Console.WriteLine("=> Using new to create variables:");

            bool b = new bool();
            System.Int32 i = new System.Int32();
            double d = new double();
            DateTime dt = new DateTime();

            Console.WriteLine("{0}, {1}, {2}, {3}", b, i, d, dt);

            Console.WriteLine();
            Console.ReadLine();
            Console.Clear();
        }

        private static void LocalVarDeclarations()
        {
            Console.WriteLine("=> Data Declarations:");

            int myInt = 0;
            string myString;
            myString = "This is my character data";
            bool b1 = true, b2 = false, b3 = b1;
            System.Boolean b4 = false;

            Console.WriteLine("Your data: {0}, {1}, {2}, {3}, {4}, {5}", myInt, myString, b1, b2, b3, b4);

            Console.WriteLine();
            Console.ReadLine();
            Console.Clear();
        }
    }
}
