﻿using System;

namespace ObjectOverrides
{
    class Program
    {
        static void Main()
        {
            Console.WriteLine("***** Fun with System.Object *****\n");

            Person p1 = new Person();

            Console.WriteLine("\nToString: {0}", p1.ToString());
            Console.WriteLine("\nHash code: {0}", p1.GetHashCode());
            Console.WriteLine("\nType: {0}", p1.GetType());

            Person p2 = p1;
            object o = p2;

            if (o.Equals(p1) && object.Equals(p2, p1))
            {
                Console.WriteLine("\nSame instance!");
            }

            Person p3 = new Person("Homer", "Simpson", 50);
            Person p4 = new Person("Homer", "Simpson", 50);

            Console.WriteLine("\np3.TOString() = {0}", p3.ToString());
            Console.WriteLine("p4.ToString() = {0}", p4.ToString());

            Console.WriteLine("\np3 = p4?: {0}", p3.Equals(p4));

            Console.WriteLine("\nSame hash codes?: {0}\n", p3.GetHashCode() == p4.GetHashCode());

            p3.Age = 45;

            Console.WriteLine("\np3.TOString() = {0}", p3.ToString());
            Console.WriteLine("p4.ToString() = {0}", p4.ToString());

            Console.WriteLine("\np3 = p4?: {0}", p3.Equals(p4));

            Console.WriteLine("\nSame hash codes?: {0}", p3.GetHashCode() == p4.GetHashCode());

            Person p5 = new Person("Sally", "Jones", 4);
            Person p6 = new Person("Sally", "Jones", 4);

            Console.WriteLine("p3 and p4 have same state: {0}", object.Equals(p5, p6));
            Console.WriteLine("p3 and p4 are pointing to same object: {0}", object.ReferenceEquals(p5, p6));

            Console.ReadLine();
        }
    }
}
