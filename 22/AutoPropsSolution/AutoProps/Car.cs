﻿using System;

namespace AutoProps
{
    class Car
    {
        public string PetName { get; set; }
        public int Speed { get; set; }
        public string Color { get; set; }

        public void DisplayStats()
        {
            Console.WriteLine("Car Name: {0}\n" +
                "Speed: {1}\n" +
                "Color: {2}", PetName, Speed, Color);
        }
    }
}
