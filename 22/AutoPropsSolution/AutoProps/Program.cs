﻿using System;

namespace AutoProps
{
    class Program
    {
        static void Main()
        {
            Console.WriteLine("***** Fun with Automatic Properties *****\n\n");

            #region Simple manipulations with object and auto properties
            Car c = new Car();
            c.PetName = "Frank";
            c.Speed = 55;
            c.Color = "Red";

            Console.WriteLine("Your car is named {0}? That's odd...", c.PetName);

            c.DisplayStats();
            #endregion

            #region Demonstrate standart values in closed hide data members
            Garage g = new Garage();
            Console.WriteLine("Number of Cars: {0}", g.NumberOfCars);
            Console.WriteLine(g.MyAuto.PetName);
            #endregion

            #region Use constructor for transfer reference on object
            Garage myGarage = new Garage(c, 1);
            Console.WriteLine("Number of Cars in garage: {0}\n" +
                "Your car is named: {1}", myGarage.NumberOfCars, myGarage.MyAuto.PetName); 
            #endregion

            Console.ReadLine();
        }
    }
}
