﻿using System;

namespace FunWithStructures
{
    class Program
    {
        static void Main()
        {
            Console.WriteLine("***** A First Look at Structures *****\n\n");

            Point myPoint = new Point();
            myPoint.X = 349;
            myPoint.Y = 76;
            myPoint.Display();

            myPoint.Increment();
            myPoint.Display();

            Point p1 = new Point(50, 60);
            p1.Display();

            Console.ReadLine();
        }
    }

    struct Point
    {
        public int X;
        public int Y;

        public Point(int XPos, int YPos)
        {
            X = XPos;
            Y = YPos;
        }

        public void Increment()
        {
            X++;
            Y++;
        }

        public void Decrement()
        {
            X--;
            Y--;
        }

        public void Display()
        {
            Console.WriteLine("X = {0}, Y = {1}", X, Y);
        }
    }
}
