﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FunWithStrings
{
    class Program
    {
        static void Main()
        {
            FunWithStringBuilder();
            StringsAreImmutable2();
            StringsAreImmutable();
            StringEqaulity();
            EscapeChars();
            StringConcatenation();
            BasicStringFunctionality();
        }

        private static void FunWithStringBuilder()
        {
            Console.WriteLine("=> Using the StringBuilder:");

            StringBuilder sb = new StringBuilder("**** Fantastic Games ****");
            sb.Append("\n");
            sb.AppendLine("Half Life");
            sb.AppendLine("Morrowind");
            sb.AppendLine("Deus Ex" + "2");
            sb.AppendLine("System Shock");

            Console.WriteLine(sb.ToString());

            sb.Replace("2", " Invisible War");

            Console.WriteLine(sb.ToString());
            Console.WriteLine("sb has {0} chars.", sb.Length);

            Console.ReadLine();
            Console.Clear();
        }

        private static void StringsAreImmutable2()
        {
            string s2 = "My other string";
            Console.WriteLine(s2);
            s2 = "New string value";
            Console.WriteLine(s2);

            Console.ReadLine();
            Console.Clear();
        }

        private static void StringsAreImmutable()
        {
            string s1 = "This is my string.";
            Console.WriteLine("s1 = {0}", s1);

            string upperstring = s1.ToUpper();
            Console.WriteLine("upperstring = {0}", upperstring);

            Console.WriteLine("s1 = {0}", s1);

            Console.ReadLine();
            Console.Clear();
        }

        private static void StringEqaulity()
        {
            Console.WriteLine("=> String equality:");

            string s1 = "Hello!";
            string s2 = "Yo!";

            Console.WriteLine("s1 = {0}", s1);
            Console.WriteLine("s2 = {0}\n", s2);

            Console.WriteLine("s1 == s2: {0}", s1 == s2);
            Console.WriteLine("s1 == Hello!: {0}", s1 == "Hello!");
            Console.WriteLine("s1 == HELLO!: {0}", s1 == "HELLO!");
            Console.WriteLine("s1 == hello!: {0}", s1 == "hello!");
            Console.WriteLine("s1.Equals(s2): {0}", s1.Equals(s2));
            Console.WriteLine("Yo.Equals(s2): {0}", "Yo!".Equals(s2));

            Console.ReadLine();
            Console.Clear();
        }

        private static void EscapeChars()
        {
            Console.WriteLine("=> Escape characters:\a");

            string strWithTabs = "Model\tColor\tSpeed\tPet Name\a";
            Console.WriteLine(strWithTabs);

            Console.WriteLine("Everyone loves \"Hello World\"\a");
            Console.WriteLine("C:\\MyApp\\bin\\Debug\a");
            Console.WriteLine("All finished. \n\n\n\a ");

            Console.WriteLine(@"C:\MyApp\bin\Debug");

            string myLongString = @"This is a very
                very
                    very
                        very long string";
            Console.WriteLine(myLongString);

            Console.WriteLine(@"Cerebus said ""Darrrr! Pret-ty sun-sets""");

            Console.WriteLine(String.Format(@"\{0} + ""{1} + 
                {2:d3}", strWithTabs, myLongString, 23));

            Console.ReadLine();
            Console.Clear();
        }

        private static void StringConcatenation()
        {
            Console.WriteLine("=> String concatenation:");

            string s1 = "Programming the ";
            string s2 = "PsychoDrill (PTP)";
            string s3 = s1 + s2;
            //string s3 = String.Concat(s1, s2);

            Console.WriteLine(s3);

            Console.ReadLine();
            Console.Clear();
        }

        private static void BasicStringFunctionality()
        {
            Console.WriteLine("=> Basic String functionality:");

            string firstName = "Freddy";

            Console.WriteLine("Value of firstName: {0}", firstName);
            Console.WriteLine("firstName has {0} characters.", firstName.Length);
            Console.WriteLine("firstName in uppercase: {0}", firstName.ToUpper());
            Console.WriteLine("firstName in lowercase: {0}", firstName.ToLower());
            Console.WriteLine("firstName contains the letter 'y'?: {0}", firstName.Contains("y"));
            Console.WriteLine("firstName after replace: {0}", firstName.Replace("dy", "chik"));

            Console.ReadLine();
            Console.Clear();
        }
    }
}
