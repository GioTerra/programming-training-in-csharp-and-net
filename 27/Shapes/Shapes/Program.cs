﻿using System;

namespace Shapes
{
    class Program
    {
        static void Main()
        {
            Console.WriteLine("***** Fun with Polymorphism *****\n\n");

            Hexagon hex = new Hexagon("Beth");
            hex.Draw();

            Circle cir = new Circle("Cindy");
            cir.Draw();

            Shape[] myShapes = new Shape[] 
            {
                new Hexagon(),
                new Circle(),
                new Hexagon("Mick"),
                new Circle("Beth"),
                new Hexagon("Linda")
            };

            foreach (var s in myShapes)
            {
                s.Draw();
            }

            ThreeDCircle o = new ThreeDCircle();
            o.Draw();

            ((Circle)o).Draw();

            Console.ReadLine();
        }
    }
}
