﻿using System;

namespace Shapes
{
    class ThreeDCircle : Circle
    {
        public new void Draw()
        {
            Console.WriteLine("Drawing a 3D Circle");
        }

        protected new string PetName;
    }
}
