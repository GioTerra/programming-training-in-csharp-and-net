﻿using System;

namespace BasicConsoleIO
{
    class Program
    {
        static void Main()
        {
            Console.WriteLine("***** Basic Console I/O *****");

            GetUserData();

            FormatNumericalData();
        }

        //Использовать несколько дескрипторов формата.
        private static void FormatNumericalData()
        {
            Console.WriteLine("The value 99999 in various formats: ");
            Console.WriteLine("c format: {0:c}", 99999);
            Console.WriteLine("d9 format: {0:d9}", 99999);
            Console.WriteLine("f3 format: {0:f3}", 99999);
            Console.WriteLine("n format: {0:n}", 99999);
            Console.WriteLine("E format: {0:E}", 99999);
            Console.WriteLine("e format: {0:e}", 99999);
            Console.WriteLine("X format: {0:X}", 99999);
            Console.WriteLine("x format: {0:x}", 99999);

            Console.ReadLine();
            Console.Clear();
        }

        private static void GetUserData()
        {
            #region GiveInformationAboutYourself
            //Получить информацию об имени и возрасте.
            Console.WriteLine("Please enter your name: "); //Запрос на ввод имени
            Console.Beep(1000, 1000);
            string userName = Console.ReadLine();

            Console.WriteLine("Please enter your age: "); //Запрос на ввод возраста
            Console.Beep(2000, 1000);
            string userAge = Console.ReadLine();
            #endregion

            #region CheatColorForeground
            //Изменить цвет переднего плана
            ConsoleColor prevColor = Console.ForegroundColor;
            Console.ForegroundColor = ConsoleColor.Yellow; 
            #endregion

            //Вывести полученные сведения на консоль
            Console.WriteLine("Hello {0}! You are {1} years old.", userName, userAge);

            //Восстановить предыдущий цвет переднего плана
            Console.ForegroundColor = prevColor;

            Console.ReadLine();
            Console.Clear();
        }
    }
}
