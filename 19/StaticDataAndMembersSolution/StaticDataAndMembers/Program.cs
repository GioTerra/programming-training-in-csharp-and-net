﻿using System;

namespace StaticDataAndMembers
{
    class Program
    {
        static void Main()
        {
            Console.WriteLine("***** Fun with Static Data *****\n");

            SavingsAccount s1 = new SavingsAccount(50);
            Console.WriteLine("Interest Rate is: {0}", SavingsAccount.InterestRate);
            SavingsAccount.InterestRate = 0.08;
            
            SavingsAccount s2 = new SavingsAccount(100);
            Console.WriteLine("Interest Rate is: {0}", SavingsAccount.InterestRate);

            SavingsAccount s3 = new SavingsAccount(10000.75);
            Console.WriteLine("Interest Rate is: {0}", SavingsAccount.InterestRate);

            Console.ReadLine();
        }
    }
}
