﻿using System;

namespace StaticDataAndMembers
{
    class SavingsAccount
    {
        public double currBalance;
        public static double currInterestRate;

        public SavingsAccount(double balance)
        {
            currBalance = balance;
        }

        static SavingsAccount()
        {
            Console.WriteLine("In static ctor!");
            currInterestRate = 0.04;
        }

        public static void SetInterestRate(double newRate)
        {
            currInterestRate = newRate;
        }

        public static double GetInterestRate()
        {
            return currInterestRate;
        }

        public static double InterestRate
        {
            get
            {
                return currInterestRate;
            }
            set
            {
                currInterestRate = value;
            }
        }
    }
}
